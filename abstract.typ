#let abstract_page(
  lang_dict: {},
  keywords: (),
  body
) = {
  set heading(numbering: none, outlined: false)
  pagebreak(to: "odd", weak: true)
  if keywords == none {
    panic("keywords must be speciied together with the abstract")
  }
  [ = #lang_dict.abstract ]

  body

  v(1cm)

  text(weight: "bold", lang_dict.keywords + ": ")
  for kw in keywords {[#kw, ]}
}
