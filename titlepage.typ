
#let titlepage(
  lang_dict: {},
  language: "",
  title: "",
  titleGerman: "",
  degree: "",
  supervisors: (),
  author: "",
  matrNr: "",
  submission_date: "",
) = {
  set page(
    margin: (left: 17.5mm, right: 17.5mm, top: 20.75mm, bottom: 22.75mm),
    numbering: none,
    number-align: center,
  )

  let sans-font = "CMU Sans Serif"
  set text(
    font: "CMU Serif",
    weight: 200,
    size: 11pt,
    lang: "en"
  )
  set block(spacing: 0.8em)

  set par(leading: 0.75em)

  align(right, image(height: 1.6cm, "logos/IHTA_bildmarke_und_text_EN.svg"))
  lang_dict.institute_text

  v(2.6cm)
  // --- Title Page ---
  if language == "en" {
    text(lang: "en", hyphenate: false, align(center, text(1.6em, weight: "bold", title)))
    //v(0.2cm)
    text(lang: "de", hyphenate: false, align(center, text(luma(128), 1.6em, weight: "bold", titleGerman)))
  } else if language == "de" {
    text(lang: "de", hyphenate: false, align(center, text(font: sans-font, 1.6em, weight: "bold", titleGerman)))
    text(lang: "en", hyphenate: false, align(center, text(luma(128), font: sans-font, 1.6em, weight: "bold", title)))
  } else {
    panic("Unsupported language: " + language)
  }

  v(10mm)

  align(center, text(1.5em, weight: "bold", font: sans-font,
    if degree == "Bachelor" {
      lang_dict.thesis_name_ba
    } else if degree == "Master" {
      lang_dict.thesis_name_ma
    } else {
      panic("Unknown degree: " + degree)
    }
  ))

  v(11mm)

  align(center, text(1.5em, author))
  align(center, text(1.5em, lang_dict.matr_nr + ": " + matrNr))

  v(4mm)

  align(center, text(1.5em, submission_date))

  v(3.0cm)

  align(center,
    text(1.5em,
      stack(spacing: 1em, lang_dict.supervisor + ":", ..supervisors)
    )
  )

  v(7.5mm)

  align(center, text(1.1em,
    lang_dict.institute_name
  ))

  pagebreak()
}
