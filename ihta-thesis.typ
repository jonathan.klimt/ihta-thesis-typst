// ========================================================
//    IHTA Thesis Template
//    by Jonathan Klimt (ACS - RWTH-Aachen)
// ========================================================

#import "titlepage.typ": titlepage
#import "abstract.typ": abstract_page

// We provide a dict for every supported language from which the correct
// translation of the keywords can be taken
#let de_keywords = (
  abstract: "Kurzfassung",
  keywords: "Stichwörter",
  matr_nr: "Matrikelnummer",
  thesis_name_ba: "Bachelorarbeit",
  thesis_name_ma: "Masterarbeit",
  institute_name: "Institut für Hörtechnik und Akkustik
Prof. Dr.-Ing Janina Fels
RWTH Aachen University",
  institute_text: "Diese Arbeit wurde vorgelegt am Institut für Hörtechnik und Akustik",
  supervisor: "Betreuer",
  examiner: "Gutachter",
  contents: "Inhaltsverzeichnis",
  list_of_figures: "Abbildungsverzeichnis",
  list_of_tables: "Tabellenverzeichnis",
  list_of_listings: "Verzeichnis der Codeabschnitte",
  //bibliography: "Literaturverzeichnis",
)

#let en_keywords = (
  abstract: "Abstract",
  keywords: "Keywords",
  matr_nr: "Matriculation Number",
  thesis_name_ba: "Bachelor Thesis",
  thesis_name_ma: "Master Thesis",
  institute_name: "Institute for Hearing Technology and Acousitcs
Prof. Dr.-Ing Janina Fels
RWTH Aachen University",
  institute_text: "This work was presented at the Institute for Hearing Technology and Acoustics",
  supervisor: "Supervisors",
  examiner: [$1^("st")$ Examiner],
  contents: "Table of Contents",
  list_of_figures: "List of Figures",
  list_of_tables: "List of Tables",
  list_of_listings: "List of Listings",
)

/// The main configuration of the document. Must be called at the beginning of
/// the thesis document as follows:
///
/// ```typ
/// #show: thesis_settings.with(title: "blah", ...)
/// ```
#let thesis_settings(
  language: "en",
  title: "",
  titleGerman: "",
  degree: "",
  supervisors: (),
  author: "",
  matrNr: "",
  abstract_de: "",
  keywords_de: (),
  abstract_en: none,
  keywords_en: none,
  list_of_figures: true,
  list_of_tables: true,
  list_of_listings: true,
  submission_date: "Januar 1, 2024",
  body,
) = {
  set document(title: title, author: author)
  set page(
    margin: (inside: 25mm, outside: 20mm, top: 30mm, bottom: 25mm),
    number-align: center,
    binding: left,
  )

  set page(
    numbering: "i",
    footer: []
  )

  let body-font = "CMU Serif"
  let sans-font = "CMU Sans Serif"

  set text(
    font: body-font,
    weight: 300,
    size: 11pt,
    lang: language
  )

  show math.equation: set text(weight: 400)

  let lang_dict = if language == "en" {
    en_keywords
  } else if language == "de" {
    de_keywords
  } else {
    panic("Unknown language: " + language)
  }

  // --- Front Matter ---

  titlepage(
    lang_dict: lang_dict,
    language: language,
    title: title,
    titleGerman: titleGerman,
    degree: degree,
    supervisors: supervisors,
    author: author,
    matrNr: matrNr,
    submission_date: submission_date,
  )
  counter(page).update(1)

  // --- Headings ---

  // We must set these settings after the admonition
  show heading: set text(font: sans-font)
  show heading: set block(above: 2em, below: 1em)
  set heading(numbering: "1.1", bookmarked: true)

  // As first level headings are "chapters", we make them extra large and add
  // space below
  show heading.where(level: 1): it => [
      #pagebreak(to: "odd", weak: true)
      #v(3cm)
      #text(1.35em, weight: "bold", it)
      #v(1cm)
  ]
  show heading.where(level: 2): set text(1.25em, weight: "bold")
  show heading.where(level: 3): set text(1.2em, weight: "bold")

  // Reference first-level headings as "chapters"
  show ref: it => {
    let el = it.element
    if el != none and el.func() == heading and el.level == 1 {
      [Chapter ]
      numbering(
        el.numbering,
        ..counter(heading).at(el.location())
      )
    } else {
      it
    }
  }

  // --- Paragraphs ---
  set par(leading: 2.5mm, first-line-indent: 0.0em, justify: true)
  show par: set block(spacing: 1.10em)

  // --- Citations ---
  set cite(style: "ieee")

  // --- Figures ---
  show figure: set text(size: 0.85em)

  // --- Lists ---
  // Increase the spacing in lists, but not by the ridiculous amount in the original template
  set enum(spacing: 2em)
  set list(spacing: 2em, indent: 1cm,  marker: (text(size: 8pt, [•]),[–],pad(top: 0.2em,[\*])))
  // list spacing is still a little broken: https://github.com/typst/typst/issues/1715

  // --- Abstracts ---
  abstract_page(lang_dict: de_keywords, keywords: keywords_de, abstract_de)
  if abstract_en != none {
    abstract_page(lang_dict: en_keywords, keywords: keywords_en, abstract_en)
  }

  // --- Table of Contents ---
  {
    // TODO: The spacing between the element level and the title is too small
    show outline.entry.where(
      level: 1
    ): it => {
      // Kudos to https://stackoverflow.com/a/77085040/6551168
      if it.at("label", default: none) == <modified-entry> {
        it // prevent infinite recursion
      } else {
        pad(bottom: -7.5mm, top: 1mm, // I have no clue, why a negative padding is necessary here...
          text(font: sans-font, 12pt, weight: "bold",
            [#outline.entry( it.level, it.element, [#it.body], [], it.page) <modified-entry>]
          )
        )
      }
    }
    outline(
      title: {
        text(font: sans-font, 1.0em, weight: 700, lang_dict.contents)
      },
      indent: 2em
    )
  }


  // --- Header and Footer ---
  let last_chap_name = state("page-last-chap", [])
  let last_sec_name = state("page-last-section", [])
  set page(
    header-ascent: 30%,
    footer-descent: 30%,
    header: locate(
      loc => {
        // The header is a bit tricky. We want the current chapter name on the
        // left page and the current section name on the right. This is still
        // not perfect, as it pics the last section on the double page instead
        // of the first one.

        // find last heading of level 1 on current page
        let last-chapter = query( heading.where(level: 1), loc).rev().find(h => h.location().page() == loc.page())
        let last-section = query( heading.where(level: 2), loc).rev().find(h => h.location().page() == loc.page())

        if last-section != none {
          last_sec_name.update(
            [
              #counter(heading).at(last-section.location()).map(h => [#h]).join(".") -
              #last-section.body
            ]
          )
        }

        if last-chapter != none {
          // There is a chapter beginning on the page. Update the page but don't print a header
          last_chap_name.update(
            block(
              stroke: (bottom: 0.4pt + black),
              width: 100%,
              inset: (y: 3mm),
              [
                #counter(heading).at(last-chapter.location()).map(h => [#h]).join(".") -
                #last-chapter.body
              ]
            )
          )
        } else {
          // no headings on the page, use last heading from variable
          if calc.even(counter(page).at(loc).first()) {
            align(left, text(weight: "medium", last_chap_name.display()))
          } else {
            block(
              stroke: (bottom: 0.4pt + black),
              width: 100%,
              inset: (y: 3mm),
              align(bottom,
                grid(columns: (1fr, 1fr),
                  text(weight: "medium", last_sec_name.display()),
                  align(right, image(height: 0.6cm, "logos/IHTA_bildmarke.svg"))
                )
              )
            )
          }
        }
      }
    ),
    footer: locate(loc => {
      block(
        stroke: (top: 0.4pt + black),
        width: 100%,
        inset: (y: 3mm),
        {
          let n = counter(page).at(loc).first()
          set align(if calc.rem(n, 2) == 0 { left } else { right })
          counter(page).display("1")
        }
      )
    })
  )

  pagebreak()
  set page(numbering: "1")
  counter(page).update(1)

  // --- Main body ---
  set par(justify: true)

  body

  // --- List of figures ---
  if list_of_figures {
    pagebreak(to: "odd", weak: true)
    outline(
      title: lang_dict.list_of_figures,
      target: figure.where(kind: image),
    )
  }

  // --- List of tables ---
  if list_of_tables {
    pagebreak(to: "odd", weak: true)

    outline(
      title:  lang_dict.list_of_tables,
      target: figure.where(kind: table)
    )
  }

  // --- List of listings ---
  if list_of_listings {
    pagebreak(to: "odd", weak: true)
    outline(
      title:  lang_dict.list_of_listings,
      target: figure.where(kind: raw),
    )
  }

  // --- Bibliography ---
  pagebreak(to: "odd", weak: true)
  bibliography(full: false, "../thesis.bib" )
}

